#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
void menu();
void Calculator_Menu();
int add (int arg_1 , int arg_2);
int sub(int arg_1, int arg_2);
int power(int arg_1, int arg_2);
int mul (int arg_1 , int arg_2);
int division (int arg_1 , int arg_2);
int mod (int arg_1 , int arg_2);
int doit (int func(), int arg_1, int arg_2);
double calculate_homework(int Hw[]);
int take_grades(int Hw[]);
int take_exam_grades(int Exam[]);
double calculate_lab(int Lab[]);
double calculate_all(int Hw ,int Exam[],int Lab);
void Print_the_Shape(int height);

int main(){
	
	menu();


	}



void menu() {                                                           //This is menu for parts
	int Hw_Grades[10];
	int Lab_Grades[10];
	int Midterm_Final[2];
	int Hw,Lab;
	int height;

  	 int choice =0;
  	 


     while(choice!=4){
     		 printf("***** MENU *****\n");
    		 printf("1.Calculator\n2.Grades\n3.Diamond Hallow\n4.Exit\n");
     		 printf("choice (1-4) :");
     		 scanf("%d",&choice);

     		switch(choice){
					case 1:
						Calculator_Menu();                             // i implemented a function for just calculator. Because we are using swtich case too.                  
						break;
					case 2:
							take_grades(Hw_Grades);
							take_grades(Lab_Grades);												// this is all abaout for take and calculate the grades.
							take_exam_grades(Midterm_Final);
							Hw = calculate_homework(Hw_Grades);
							Lab = calculate_lab(Lab_Grades);
							printf("Your grade is : %lf \n",calculate_all(Hw,Midterm_Final,Lab));
						break;
					case 3:
							printf("Please enter the height of shape\n");								//and this is for drawing shape.
							scanf("%d",&height);
							Print_the_Shape(height);
						break;
					case 4:
						return;
						break;	     				


				
     		}

     

     }


}

void Calculator_Menu(){  // This is my calculating menu. You wanted us to make a menu function for calculator.
	char *line;
	int arg_1=0,arg_2=0;
	int i=0;
	const char space[2] = " ";
	char operation[100];
	char *ptr;
	char op[2];
	char temp[100];
	int res = 0;
	int choice_cal=0;					
						
	printf("****** CALCULATOR INFORMATION *******\n");
	printf(	"1.You can use '+','-','/','*','%%','**' operattions \n");									// this is introduction for using the calculator
	printf(	"2.You should write operator first\n");
	printf(	"3.Then you can enter the numbers that you want to use\n");
	printf(	"4.You should enter space ' ' between operations and numbers\n");
	printf(	"For ex:+ 8 2\n");
	printf("                                                press 'e' or 'E' for Exit\n");



						while(choice_cal != 69 && choice_cal !=101 ){
									
									scanf(" %[^\n]",operation);												// first of all i take the input line from user
									
									strcpy(temp,operation);
									ptr = strtok(operation, space);											// i deleted the spaces in this array
									
								
									choice_cal = *ptr + *(++ptr);											// This is for swtich case intiger values of operators
										
									fflush(stdin);															// This is for cleaning buffer. I used because we use a lot of scanf()
									
   									while( ptr != NULL ) {
      								
    
      								ptr = strtok(NULL, space);
  									i++;
  									 }
									
							switch(choice_cal){
								case 43:																	//43 is intiger value of "+"
								


  									 
  									 if( i==3){																//if user enter two intiger this if will be actived
  									 	i = 0;
  									 	sscanf(temp,"%s %d %d",op,&arg_1,&arg_2);
  									 	
  									 	res = doit(add,arg_1,arg_2);
  									 	printf("%d",res);
  									 }
  									 else if(i==2){															//if user enter one intiger this if will be actived
  									 	i = 0;
  									 	
  									 	sscanf(temp,"%s %d",op,&arg_1);
  									 	res = doit(add,arg_1,res);
										printf("%d ",res);


  									 }


									break;
								case 45:                                                             	//45 is intiger value of "-"

  									 if( i==3){
  									 	i = 0;
  									 	sscanf(temp,"%s %d %d",op,&arg_1,&arg_2);
  									 	
  									 	res = doit(sub,arg_1,arg_2);
  									 	printf("%d",res);
  									 }
  									 else if(i==2){
  									 	i = 0;
  									 	
  									 	sscanf(temp,"%s %d",op,&arg_1);
  									 	res = doit(sub,res,arg_1);
										printf("%d ",res);


  									 }
									break;
								case 47:                                                            	//47 is intiger value of "/"

  									 if( i==3){
  									 	i = 0;
  									 	sscanf(temp,"%s %d %d",op,&arg_1,&arg_2);
  									 	
  									 	res = doit(division,arg_1,arg_2);
  									 	printf("%d",res);
  									 }
  									 else if(i==2){
  									 	i = 0;
  									 	
  									 	sscanf(temp,"%s %d",op,&arg_1);
  									 	res = doit(division,res,arg_1);
										printf("%d ",res);


  									 }
									break; 
								case 42:                                                          	//42 is intiger value of "*"

  									 
  									 if( i==3){
  									 	i = 0;
  									 	sscanf(temp,"%s %d %d",op,&arg_1,&arg_2);
  									 	
  									 	res = doit(mul,arg_1,arg_2);
  									 	printf("%d",res);
  									 }
  									 else if(i==2){
  									 	i = 0;
  									 	
  									 	sscanf(temp,"%s %d",op,&arg_1);
  									 	res = doit(mul,arg_1,res);
										printf("%d ",res);


  									 }
									break;
								case 37:                                                           	//37 is intiger value of "%"

  									 
  									 if( i==3){
  									 	i = 0;
  									 	sscanf(temp,"%s %d %d",op,&arg_1,&arg_2);
  									 	
  									 	res = doit(mod,arg_1,arg_2);
  									 	printf("%d",res);
  									 }
  									 else if(i==2){
  									 	i = 0;
  									 	
  									 	sscanf(temp,"%s %d",op,&arg_1);
  									 	res = doit(mod,res,arg_1);
										printf("%d",res);


  									 }
									break;
								case 84:                                                         	//84 is intiger value of "**"

  									 
  									 if( i==3){
  									 	i = 0;
  									 	sscanf(temp,"%s %d %d",op,&arg_1,&arg_2);
  									 	
  									 	res = doit(power,arg_1,arg_2);
  									 	printf("%d\n",res);
  									 }
  									 else if(i==2){
  									 	i = 0;
  									 	
  									 	sscanf(temp,"%s %d",op,&arg_1);
  									 	res = doit(power,res,arg_1);
										printf("%d",res);


  									 }
									break;
			
								case 69:                                                        	//69 is intiger value of "E"
									
									return;
									break;

							}
							printf("\n");
						}
}


int add (int arg_1 , int arg_2){									//  THIS FUNCTIONS IS JUST MATH
	

	
	return arg_1+arg_2;
} 


int sub(int arg_1, int arg_2){
	
	return arg_1-arg_2;
}
int power(int arg_1, int arg_2){
	
	return pow(arg_1,arg_2);
}
int mul (int arg_1 , int arg_2){
	

	
	return arg_1*arg_2;
}

int division (int arg_1 , int arg_2){
	
	
	
	return arg_1/arg_2;
}

int mod (int arg_1 , int arg_2){


	
	return arg_1%arg_2;
}


int doit (int func(),int arg_1, int arg_2){
	return func(arg_1, arg_2);
}




int take_grades(int Grade[]){														//this is for taking homework and lab grades 
	int i;
		for(i=0;i<10;i++){
			printf("Please enter %d. Grade\n",i+1 );
			scanf("%d",&Grade[i]);
		}


		printf("\n\n");

		
}

int take_exam_grades(int Exam[]){											// this is for exam grades 
	int i;
		for(i=0;i<2;i++){
			printf("Enter The Midterm and Final Grades\n");
			scanf("%d",&Exam[i]);
		}



}


double calculate_homework(int Hw[]){										// calculating hw avarage
	int i,sum=0;
		for(i=0;i<10;i++){
			sum += Hw[i];

		}

		return sum/10;
}



double calculate_lab(int Lab[]){										// calculating lab avarege
	int i,sum=0;
		for(i=0;i<10;i++){
			sum += Lab[i];

		}

		return sum/10;
}


double calculate_all(int Hw ,int Exam[],int Lab){						// and this is final result for print
	int  Midterm,Final;
	Hw = (Hw*10)/100;
	Lab = (Lab*20)/100;
	Midterm = (Exam[0]*30)/100;
	Final = (Exam[1]*40)/100;

	double result = (double)(Hw+Lab+Midterm+Final);

	return result;




}


void Print_the_Shape(int height){

	int i,j,k=1,l,m,p=0;							
	for(i=0;i<height-1;i++){									// i draw this with 'k--' an "k++"		
		printf(" ");
		}
	  printf("/\\\n");													// i use pdf's formula for this shape
	for(j=0;j<height-1;j++){
		for(i=0;i< (height-1)-k;i++){
			printf(" ");
	}
	  printf("/");                                                      //firs it put "/"
	  k++;
	  
	  	for(m=0;m< 1+p ; m++){											//then printing ** 
	  		printf("**");
	  	}
	  	
	  	p++;
	  	printf("\\\n");													// after that it put "\" and end line
	}
	p--;
	k--;
	for(j=0;j<height-1;j++){
		for(i=0;i< (height-1)-k;i++){								//This is for second part and for lower parts
			printf(" ");

	}
	  printf("\\");
	  k--;

	for(m=0;m< 1+p ; m++){
	  		printf("**");
	  	}
	  	
	  	p--;
	  	printf("/\n");
	 } 

	for(i=0;i<height-1;i++){
		printf(" ");
		}
	  printf("\\/\n");

}



	
